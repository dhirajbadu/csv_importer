package document.csv_reader;

import com.document.core.api.impl.DealsPerOrderingCurrencyApi;
import com.document.core.model.entity.ValidDocument;
import com.document.core.model.enumconstant.Currency;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DealsPerOrderingCurrencyTest {

    @Test
    public void app(){
        Map<Currency , Long> map = new DealsPerOrderingCurrencyApi().getCountedCurrency(getDocList());

        for (Currency currency : map.keySet()){
            System.out.println(currency.toString() + " : " + map.get(currency));
        }
    }

    @Test
    public void dublicateTest(){
        List<ValidDocument> documentList = getDocList();

        //List<ValidDocument> uniqueData = documentList.stream().filter(n -> documentList.stream().filter(x -> x.getFrmCurrencyISO() == n.getFrmCurrencyISO()).count() > 1).collect(Collectors.toList());
        List<ValidDocument> uniqueData = documentList.stream().filter(distinctByKey(b -> b.getFrmCurrencyISO())).collect(Collectors.toList());

        System.out.println(documentList.size() + " : " + uniqueData.size());

    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private List<ValidDocument> getDocList(){
        List<ValidDocument> documentList  = new ArrayList<>();

        documentList.add(getDoc(Currency.AFGHANISTAN));
        documentList.add(getDoc(Currency.AFGHANISTAN));
        documentList.add(getDoc(Currency.AFGHANISTAN));
        documentList.add(getDoc(Currency.ALBANIA));
        documentList.add(getDoc(Currency.ALBANIA));
        documentList.add(getDoc(Currency.AUSTRALIA));

        return documentList;
    }

    private ValidDocument getDoc(Currency currency){
        ValidDocument document = new ValidDocument();

        document.setFrmCurrencyISO(currency);

        return document;
    }
}
