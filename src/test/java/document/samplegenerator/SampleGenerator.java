package document.samplegenerator;

import com.document.core.util.FileHandler;
import org.junit.Test;

import java.io.IOException;

public class SampleGenerator {

    @Test
    public void app(){
        try {
            FileHandler.csvSampleGenerator();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
