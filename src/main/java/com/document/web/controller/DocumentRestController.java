package com.document.web.controller;

import com.document.core.api.iapi.IDealsPerOrderingCurrencyApi;
import com.document.core.api.iapi.IImporterApi;
import com.document.core.model.entity.DealsPerOrderingCurrency;
import com.document.core.model.entity.InvalidDocument;
import com.document.core.model.entity.ValidDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ajax/document")
public class DocumentRestController {

    @Autowired
    private IImporterApi importerApi;

    @Autowired
    private IDealsPerOrderingCurrencyApi dealsPerOrderingCurrencyApi;

    @GetMapping("/validlist")
    public Map<String, Object> validList(@RequestParam("length") Integer max,
                                        @RequestParam("start") Integer offset,
                                        @RequestParam("order[0][column]") String sort,
                                        @RequestParam("order[0][dir]") String order) {

        Map<String, Object> resultMap = new HashMap<>();

        List<ValidDocument> dataResultList = importerApi.listValid(max, offset, order, sort);
        List<String[]> arrStr = new ArrayList<>();

        for (ValidDocument result : dataResultList) {
            String[] arr = new String[6];


            arr[0] = result.getDealId();
            arr[1] = result.getFrmCurrencyISO().toString();
            arr[2] = result.getToCurrencyISO().toString();
            arr[3] = result.getTimestamp().toString();
            arr[4] = result.getAmount().toString();
            arr[5] = result.getFileName();
            arrStr.add(arr);
        }


        resultMap.put("data", arrStr);
        resultMap.put("recordsTotal", importerApi.countValid());
        resultMap.put("recordsFiltered", importerApi.countValid());

        return resultMap;
    }


    @GetMapping("/invalidlist")
    public Map<String, Object> invalidList(@RequestParam("length") Integer max,
                                        @RequestParam("start") Integer offset,
                                        @RequestParam("order[0][column]") String sort,
                                        @RequestParam("order[0][dir]") String order) {

        Map<String, Object> resultMap = new HashMap<>();

        List<InvalidDocument> dataResultList = importerApi.listInvalid(max, offset, order, sort);
        List<String[]> arrStr = new ArrayList<>();

        for (InvalidDocument result : dataResultList) {
            String[] arr = new String[6];


            arr[0] = result.getDealId();
            arr[1] = result.getFrmCurrencyISO();
            arr[2] = result.getToCurrencyISO();
            arr[3] = result.getTimestamp();
            arr[4] = result.getAmount();
            arr[5] = result.getFileName();
            arrStr.add(arr);
        }


        resultMap.put("data", arrStr);
        resultMap.put("recordsTotal", importerApi.countInvalid());
        resultMap.put("recordsFiltered", importerApi.countInvalid());

        return resultMap;
    }

    @GetMapping("/dealcount")
    public Map<String, Object> dealCountList(@RequestParam("length") Integer max,
                                           @RequestParam("start") Integer offset,
                                           @RequestParam("order[0][column]") String sort,
                                           @RequestParam("order[0][dir]") String order) {

        Map<String, Object> resultMap = new HashMap<>();

        List<DealsPerOrderingCurrency> deals = dealsPerOrderingCurrencyApi.list(max, offset, order, sort);
        List<String[]> arrStr = new ArrayList<>();

        for (DealsPerOrderingCurrency result : deals) {
            String[] arr = new String[3];


            arr[0] = result.getId().toString();
            arr[1] = result.getOrderingCurrency().toString();
            arr[2] = result.getDealCount() + "";
            arrStr.add(arr);
        }


        resultMap.put("data", arrStr);
        resultMap.put("recordsTotal", dealsPerOrderingCurrencyApi.count());
        resultMap.put("recordsFiltered", dealsPerOrderingCurrencyApi.count());

        return resultMap;
    }
}
