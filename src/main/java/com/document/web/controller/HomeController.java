package com.document.web.controller;

import com.document.core.api.iapi.IImporterApi;
import com.document.core.util.FileHandler;
import com.document.core.validation.DocumentValidator;
import com.document.web.util.LoggerUtil;
import com.document.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {

    @Autowired
    private IImporterApi importerApi;

    @Autowired
    private DocumentValidator validator;

    @GetMapping("/")
    public String index(){
        return "/index";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("doc")MultipartFile doc , RedirectAttributes redirectAttributes) throws Exception {

        try {
            synchronized (this.getClass()) {
                String error = validator.onUpload(doc);

                if (!"".equals(error)) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR, error);
                    return "redirect:/";
                }

                String msg = importerApi.importCsv(doc);
                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.logException(this.getClass() , e);
            throw new Exception("some thing wrong");
        }

        return "redirect:/";
    }

}
