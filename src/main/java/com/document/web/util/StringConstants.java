package com.document.web.util;

public class StringConstants {

	public static final String LASTPAGE = "lastpage";
	public static final String CURRENTPAGE = "currentpage";
	public static final String PAGELIST = "pagelist";
	public static final String TERM = "term";

	public static final String ERROR = "error";
	public static final String MESSAGE = "message";
}