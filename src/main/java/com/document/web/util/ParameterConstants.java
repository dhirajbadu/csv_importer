package com.document.web.util;

import java.io.File;
import java.math.BigDecimal;

public class ParameterConstants {

    public final static String DELIMITER = ";";

	public final static String WINDOWS_PATH = System.getProperty( "user.home")+ File.separator+"Documents";
	public final static String MAC_PATH = "/Users";
	public final static String UNIX_PATH = System.getProperty( "user.home");

}
