package com.document.web.util;

import org.apache.log4j.LogManager;


public class LoggerUtil {
	
	final static org.apache.log4j.Logger LOGGER = LogManager.getLogger(LoggerUtil.class); 
	
	public static void logException(Class className,Exception exception){
		LOGGER.debug("Exception class "+className);
		LOGGER.debug("Error class "+exception.getClass());
		LOGGER.debug("Error cause "+exception.getCause());
		LOGGER.debug("Error message "+exception.getMessage());
	}

	public static void logWarn(Class className,Exception exception){
		LOGGER.warn("Exception class "+className);
		LOGGER.warn("Error class "+exception.getClass());
		LOGGER.warn("Error cause "+exception.getCause());
		LOGGER.warn("Error message "+exception.getMessage());
	}

	public static void logInfo(Class className, String info){
		LOGGER.warn("Info class " + className);
		LOGGER.warn("Info : " + info);
	}

}
