package com.document.core.util;

import com.document.core.model.enumconstant.Currency;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileHandler {

	public static String multipartSingleNameResolver(MultipartFile file) {

		if (file != null) {
			if (!file.getOriginalFilename().trim().equals("")) {
				return file.getOriginalFilename();
			} else {
				return "";
			}
		} else {
			return "";
		}

	}

	/*
	* this function is used to generate sample data
	* */
	public static void csvSampleGenerator() throws IOException {
		String path = System.getProperty("user.dir") + "/src/test/resources/sample.csv";

		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));
			 CSVPrinter csvPrinter = new CSVPrinter(writer,
					 CSVFormat.DEFAULT.withHeader("dealId",
							 "orderingCurrency", "toCurrency", "timestamp", "amount").withDelimiter(';'))) {

			//valid list
			for (int i = 0 ; i < 1000 ; i++) {
				String dealId = "001" + i;
				String orderingCurrency = Currency.AUSTRALIA.getValue();
				String toCurrency = Currency.BRAZIL.getCode();
				String timestamp = System.currentTimeMillis() + "";
				String amount = String.valueOf(1 + i);

				csvPrinter.printRecord(dealId , orderingCurrency , toCurrency , timestamp , amount);
			}

			//valid list
			for (int i = 0 ; i < 1000 ; i++) {
				String dealId = "Data064" + i;
				String orderingCurrency = Currency.BANGLADESH.getValue();
				String toCurrency = Currency.ANGOLA.getCode();
				String timestamp = System.currentTimeMillis() + "";
				String amount = String.valueOf(1 + i);

				csvPrinter.printRecord(dealId , orderingCurrency , toCurrency , timestamp , amount);
			}

			//valid list
			for (int i = 0 ; i < 1000 ; i++) {
				String dealId = "TestData064" + i;
				String orderingCurrency = Currency.CAMBODIA.getValue();
				String toCurrency = Currency.ANGOLA.getCode();
				String timestamp = System.currentTimeMillis() + "";
				String amount = String.valueOf(10 + i);

				csvPrinter.printRecord(dealId , orderingCurrency , toCurrency , timestamp , amount);
			}

			//invalid list
			for (int i = 0 ; i < 1000 ; i++) {
				String dealId = "001";
				String orderingCurrency = Currency.AUSTRALIA.getValue();
				String toCurrency = Currency.BRAZIL.getCode();
				String timestamp = System.currentTimeMillis() + "";
				String amount = String.valueOf(1 + i);

				csvPrinter.printRecord(dealId , orderingCurrency , toCurrency , timestamp , amount);
			}

			//invalid list
			for (int i = 0 ; i < 1000 ; i++) {
				String dealId = "001" + i;
				String orderingCurrency = Currency.AUSTRALIA.getValue();
				String toCurrency = "";
				String timestamp = System.currentTimeMillis() + "";
				String amount = String.valueOf(1 + i);

				csvPrinter.printRecord(dealId , orderingCurrency , toCurrency , timestamp , amount);
			}

			//invalid list
			for (int i = 0 ; i < 1000 ; i++) {
				String dealId = "001" + i;
				String orderingCurrency = Currency.BULGARIA.getValue();
				String toCurrency = "huytv";
				String timestamp = System.currentTimeMillis() + "";
				String amount = String.valueOf(1 + i);

				csvPrinter.printRecord(dealId , orderingCurrency , toCurrency , timestamp , amount);
			}

			csvPrinter.flush();
			csvPrinter.close();

		}

		System.out.println("Please find the file : " + path);
	}

}
