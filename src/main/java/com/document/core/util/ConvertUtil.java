package com.document.core.util;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class ConvertUtil {

	public static final String getValidProperty(String property , String...properties){
		String result = "fileName";

		if (property == null){
			return result;
		}

		property = property.trim();

		if ("".equals(property) || property.isEmpty()){
			return result;
		}

		int count = 0;
		for (String v : properties){
			if (property.equals(String.valueOf(count))){
				return v;
			}
			count ++;
		}

		return result;
	}

	public static  final Pageable createOffsetPageRequest(Integer max, Integer offset , String direction , String property) {

		if (max == null)
			max = 10;

		if (max > 100)
			max = 100;

		if (offset == null)
			offset = 0;

		if (direction == null)
			direction = "desc";

		Sort.Direction dir = Sort.Direction.ASC;

		if ("desc".equalsIgnoreCase(direction)){
			dir = Sort.Direction.DESC;
		}

		return  new OffsetBasedPageRequest(offset , max , dir , property);

	}
}