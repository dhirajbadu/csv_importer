package com.document.core.util;

import com.document.core.model.entity.InvalidDocument;
import com.document.web.util.LoggerUtil;
import com.document.web.util.ParameterConstants;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CsvReaderUtls {

    /**
     * this function is used to convert BufferedReader from  MultipartFile
     *
     * @Param : MultipartFile
     * @Return : BufferedReader
     * */
    private  BufferedReader reader(MultipartFile inputF) throws Exception {

        BufferedReader br = null;
        try {

            InputStream inputFS = inputF.getInputStream();

            br = new BufferedReader(new InputStreamReader(inputFS));

        } catch (FileNotFoundException e) {
            LoggerUtil.logException(this.getClass() , e);
        }

        if (br == null) {
            LoggerUtil.logInfo(this.getClass() , "null BufferedReader");
            throw new Exception("some thing wrong");
        }

        return br;
    }


    /**
     * this function is used to convert List<InvalidDocument> (means all the records on provided csv file) from  MultipartFile
     *
     * @Param : MultipartFile
     * @Return : List<InvalidDocument>
     * */
    public  List<InvalidDocument> convert(MultipartFile file) throws Exception {

        List<InvalidDocument> invalidDocumentList = mapper(file);

        return invalidDocumentList;

    }

    public  List<InvalidDocument> mapper(MultipartFile file) throws Exception {

        BufferedReader br = reader(file);

        List<InvalidDocument> invalidDocumentList = br.lines().filter(CsvReaderUtls::validate).map(line -> mamapToWerehouse(line ,file.getOriginalFilename())).collect(Collectors.toList());

        br.close();

        return invalidDocumentList;
    }

    /**
     * this function is used to identify the null String and commented line on csv file
     *
     * @Param : String
     * @Return : boolean
     * */
    private static final boolean validate(String line) {

        return line != null && !line.startsWith("#");

    }

    /**
     * this function is used to map csv files line to InvalidDocument
     *
     * @Param : String , String
     * @Return : InvalidDocument
     * */
    private static final InvalidDocument mamapToWerehouse(String line , String fileName){

        if (line == null){
            return getDefault(fileName);
        }

        if (line.isEmpty()){
            return getDefault(fileName);
        }

        String[] p = line.split(ParameterConstants.DELIMITER);

        InvalidDocument invalidDocument = new InvalidDocument();

        try {
            invalidDocument.setDealId(p[0].trim());
        }catch (Exception e){
            return getDefault(fileName);
        }
        try {
            invalidDocument.setFrmCurrencyISO(p[1].trim());
        }catch (Exception e){
            invalidDocument.setFrmCurrencyISO("");

        }
        try {
            invalidDocument.setToCurrencyISO(p[2].trim());
        }catch (Exception e){
            invalidDocument.setToCurrencyISO("");
        }

        try {

            invalidDocument.setTimestamp(p[3].trim());
        }catch (Exception e){
            invalidDocument.setTimestamp("");
        }

        try {

            invalidDocument.setAmount(p[4].trim());
        }catch (Exception e){
            invalidDocument.setAmount("");
        }
        invalidDocument.setFileName(fileName);

        return invalidDocument;
    }

    /**
     * this function is used to generate InvalidDocument with empty value and file name
     *
     * @Param : String
     *
     * @Return : InvalidDocument
     * */
    private static final InvalidDocument getDefault(String fileName) {
        InvalidDocument invalidDocument = new InvalidDocument();

        invalidDocument.setDealId("");
        invalidDocument.setFrmCurrencyISO("");
        invalidDocument.setToCurrencyISO("");
        invalidDocument.setTimestamp("");
        invalidDocument.setAmount("");
        invalidDocument.setFileName(fileName);

        return invalidDocument;
    }

}
