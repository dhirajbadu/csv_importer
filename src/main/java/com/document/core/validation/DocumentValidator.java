package com.document.core.validation;

import com.document.core.model.dto.DataResult;
import com.document.core.model.entity.InvalidDocument;
import com.document.core.model.entity.ValidDocument;
import com.document.core.model.enumconstant.Currency;
import com.document.core.model.repository.InvalidDocumentRepository;
import com.document.core.model.repository.ValidDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class DocumentValidator {

    @Autowired
    private ValidDocumentRepository validDocumentRepository;

    @Autowired
    private InvalidDocumentRepository invalidDocumentRepository;

    /**
     * this function is used to get validDocument and invalidDocument from all data from csv file
     *
     * @Param : List<InvalidDocument>
     * @Return : DataResult
     * */
    public DataResult getValidData(List<InvalidDocument> invalidDocumentList) {

        DataResult result = new DataResult();

        //get valid field
        List<InvalidDocument> validList = invalidDocumentList.stream().filter(this::isValid).filter(distinctByKey(b -> b.getDealId())).collect(Collectors.toList());

        //retaining invalid data only
        invalidDocumentList.removeAll(validList);

        result.setValidList(validList.stream().map(mapToValidDocument).collect(Collectors.toList()));
        result.setInValidList(invalidDocumentList);

        return result;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }


    private Function<InvalidDocument, ValidDocument> mapToValidDocument = (document) -> {


        ValidDocument invalidDocument = new ValidDocument();

        invalidDocument.setDealId(document.getDealId());
        invalidDocument.setFrmCurrencyISO(Currency.getEnum(document.getFrmCurrencyISO()));
        invalidDocument.setToCurrencyISO(Currency.getEnum(document.getToCurrencyISO()));
        invalidDocument.setTimestamp(Long.parseLong(document.getTimestamp()));
        invalidDocument.setAmount(formatter(document.getAmount()));
        invalidDocument.setFileName(document.getFileName());

        return invalidDocument;

    };

    public static BigDecimal formatter(String value) {
        return BigDecimal.valueOf(Double.parseDouble(value)).setScale(3, RoundingMode.CEILING);

    }

    /**
     * this function is used to validate Line from csv file
     *
     * @Param: InvalidDocument
     * @Return: boolean
     * */
    private boolean isValid(InvalidDocument invalidDocument) {

        boolean valid = true;

        valid = valid && checkFrmCurrencyISO(invalidDocument.getFrmCurrencyISO());
        valid = valid && checkToCurrencyISO(invalidDocument.getToCurrencyISO());
        valid = valid && checkTimestamp(invalidDocument.getTimestamp());
        valid = valid && checkAmmount(invalidDocument.getAmount());
        valid = valid && checkId(invalidDocument.getDealId());

        return valid;
    }

    public boolean checkFileName(String value) {
        if (isNotNull(value)) {
            if (validDocumentRepository.existsDistinctByFileName(value.trim())) {
                return false;
            } else if (invalidDocumentRepository.existsDistinctByFileName(value.trim())) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    public String onUpload(MultipartFile file) {
        if (file == null) {
            return "please choose any file";
        } else if (file.isEmpty()) {
            return "please choose any file";
        } else if (!file.getContentType().equalsIgnoreCase("text/csv")) {
            System.out.println(">>>>>>>> " + file.getContentType());
            return "please choose any csv file";
        } else if (!checkFileName(file.getOriginalFilename())) {
            return "file " + file.getOriginalFilename() + " already exist";
        }else {
            return "";
        }
    }

    private boolean checkId(String value) {
        return isNotNull(value);
    }

    private boolean checkFrmCurrencyISO(String value) {
        if (isNotNull(value)) {
            return Currency.getEnum(value) != null;
        }

        return false;
    }

    private boolean checkToCurrencyISO(String value) {
        if (isNotNull(value)) {
            return Currency.getEnum(value) != null;
        }

        return false;
    }

    private boolean checkTimestamp(String value) {
        if (!isNotNull(value)) {
            return false;
        } else if (isNumeric(value) && value.length() != 13) {
            return false;
        } else {
            try {
                Long time = Long.parseLong(value);

                new Date(time);

            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    private boolean checkAmmount(String value) {
        if (isNotNull(value)) {
            return isNumeric(value);
        }
        return false;

    }

    public boolean isNumeric(String str) {
        return isNotNull(str) && (str.trim().matches("[-\\+]?\\d+(\\.\\d+)?") || str.trim().matches("[-\\+]?+(\\.\\d+)?") || /*match a -ve number that ends with (.) */ str.trim().matches("[-\\+]?\\d+(\\.)?"));  //match a number with optional '-' and decimal.
        //or match a number with optional '-' and start with '.'
    }

    private boolean isNotNull(String value) {
        if (value == null) {
            return false;
        } else if ("".equals(value.trim()) || value.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

}
