package com.document.core.api.iapi;

import com.document.core.model.entity.DealsPerOrderingCurrency;
import com.document.core.model.entity.ValidDocument;

import java.util.List;

public interface IDealsPerOrderingCurrencyApi {
    void updateOnDeal(List<ValidDocument> documentList);

    List<DealsPerOrderingCurrency> list(int max, int offset, String direction, String property);

    long count();
}
