package com.document.core.api.iapi;

import com.document.core.model.dto.DataResult;
import com.document.core.model.entity.InvalidDocument;
import com.document.core.model.entity.ValidDocument;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IImporterApi {

    String importCsv(MultipartFile file) throws Exception;

    List<ValidDocument> listValid(int max, int offset, String direction, String property);

    long countValid();

    List<InvalidDocument> listInvalid(int max, int offset, String direction, String property);

    long countInvalid();
}
