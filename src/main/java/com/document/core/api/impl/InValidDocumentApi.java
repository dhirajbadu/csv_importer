package com.document.core.api.impl;

import com.document.core.model.entity.InvalidDocument;
import com.document.core.model.entity.ValidDocument;
import com.document.core.model.enumconstant.ParameterConstant;
import com.document.core.model.repository.InvalidDocumentRepository;
import com.document.core.model.repository.ValidDocumentRepository;
import com.document.web.util.LoggerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class InValidDocumentApi implements Runnable{

    private InvalidDocument document;

    private InvalidDocumentRepository invalidDocumentRepository;
    private InvalidDocumentRepository repository;

   public InValidDocumentApi(InvalidDocumentRepository repository){
       invalidDocumentRepository = repository;
   }
    public InValidDocumentApi(InvalidDocument document , InvalidDocumentRepository repository){
       this.document = document;
       this.repository = repository;
   }

    public void save(List<InvalidDocument> invalidDocumentList) {
        ExecutorService executor = Executors.newFixedThreadPool(ParameterConstant.threadSize);
        try {
            for (int i = 0; i < invalidDocumentList.size(); i++) {

                InValidDocumentApi obj = new InValidDocumentApi(invalidDocumentList.get(i) , invalidDocumentRepository);
                Thread thread = new Thread(obj);
                executor.execute(thread);
            }

            executor.shutdown();
            // Wait until all threads are finish
            while (!executor.isTerminated()) {
                Thread.sleep(10);
                System.out.println("file import still in progress...");
            }

        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.logException(this.getClass() ,e);
        }
    }


    @Override
    public void run() {
        repository.save(document);
    }
}
