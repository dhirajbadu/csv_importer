package com.document.core.api.impl;

import com.document.core.model.entity.ValidDocument;
import com.document.core.model.enumconstant.ParameterConstant;
import com.document.core.model.repository.ValidDocumentRepository;
import com.document.web.util.LoggerUtil;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ValidDocumentApi implements Runnable {

    private ValidDocument document;

    private ValidDocumentRepository validDocumentRepository;
    private ValidDocumentRepository repository;

    public ValidDocumentApi(ValidDocumentRepository repository) {
        validDocumentRepository = repository;
    }

    public ValidDocumentApi(ValidDocument document , ValidDocumentRepository repository) {
        this.document = document;
        this.repository = repository;
    }

    //@Transactional(propagation = REQUIRES_NEW)
    public void save(List<ValidDocument> validDocumentList) {
        ExecutorService executor = Executors.newFixedThreadPool(ParameterConstant.threadSize);
        try {
            for (int i = 0; i < validDocumentList.size(); i++) {

                ValidDocumentApi obj = new ValidDocumentApi(validDocumentList.get(i) , validDocumentRepository);
                Thread thread = new Thread(obj);
                executor.execute(thread);
            }

            executor.shutdown();
            // Wait until all threads are finish
            while (!executor.isTerminated()) {
                Thread.sleep(10);
                System.out.println("file import still in progress...");
            }

        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.logException(this.getClass(), e);
        }
    }


    @Override
    public void run() {
        repository.save(document);
    }
}
