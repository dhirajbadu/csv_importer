package com.document.core.api.impl;

import com.document.core.api.iapi.IDealsPerOrderingCurrencyApi;
import com.document.core.api.iapi.IImporterApi;
import com.document.core.model.dto.DataResult;
import com.document.core.model.entity.InvalidDocument;
import com.document.core.model.entity.ValidDocument;
import com.document.core.model.repository.InvalidDocumentRepository;
import com.document.core.model.repository.ValidDocumentRepository;
import com.document.core.util.ConvertUtil;
import com.document.core.util.CsvReaderUtls;
import com.document.core.validation.DocumentValidator;
import org.apache.xmlbeans.impl.xb.xsdschema.ListDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ImporterApi implements IImporterApi {

    @Autowired
    private ValidDocumentRepository validDocumentRepository;

    @Autowired
    private InvalidDocumentRepository invalidDocumentRepository;

    @Autowired
    private DocumentValidator validator;

    @Autowired
    private IDealsPerOrderingCurrencyApi dealsPerOrderingCurrencyApi;

    /**
     * this function takes the multipartFile csv file , read it , valid and save on database
     *
     * @Param : MultipartFile
     * @Return : import status massage
     */
    @Override
    public String importCsv(MultipartFile file) throws Exception {


        List<InvalidDocument> result = new CsvReaderUtls().convert(file);

        DataResult dataResult = validator.getValidData(result);

        if (dataResult.getValidList() != null && !dataResult.getValidList().isEmpty()) {

            new ValidDocumentApi(validDocumentRepository).save(dataResult.getValidList());
            dealsPerOrderingCurrencyApi.updateOnDeal(dataResult.getValidList());
        }

        if (dataResult.getInValidList() != null && !dataResult.getInValidList().isEmpty()) {
            new InValidDocumentApi(invalidDocumentRepository).save(dataResult.getInValidList());
        }

        return "data successfully imported<br>" +
                "with valid data " + dataResult.getValidList().size() + "<br>" +
                "and invalid data " + dataResult.getInValidList().size();
    }

    /**
     * this function takes pagination parameters and retrieves validdocument list from database
     *
     * @Param : pagination parameters
     * @Return : ValidDocument list
     */
    @Override
    public List<ValidDocument> listValid(int max, int offset, String direction, String property) {
        property = ConvertUtil.getValidProperty(property, "dealId", "frmCurrencyISO", "timestamp", "amount", "toCurrencyISO", "fileName");
        Pageable pageable = ConvertUtil.createOffsetPageRequest(max, offset, direction, property);
        return validDocumentRepository.findAll(pageable).getContent();
    }

    /**
     * this function returns the row count of validdocument table
     *
     * @Param :
     * @Return : long
     */
    @Override
    public long countValid() {
        return validDocumentRepository.count();
    }

    /**
     * this function takes pagination parameters and retrieves invaliddocument list from database
     *
     * @Param : pagination parameters
     * @Return : InvalidDocument list
     */
    @Override
    public List<InvalidDocument> listInvalid(int max, int offset, String direction, String property) {
        property = ConvertUtil.getValidProperty(property, "dealId", "frmCurrencyISO", "timestamp", "amount", "toCurrencyISO", "fileName");
        Pageable pageable = ConvertUtil.createOffsetPageRequest(max, offset, direction, property);
        return invalidDocumentRepository.findAll(pageable).getContent();
    }

    /**
     * this function returns the row count of invaliddocument table
     *
     * @Param :
     * @Return : long
     */
    @Override
    public long countInvalid() {
        return invalidDocumentRepository.count();
    }
}
