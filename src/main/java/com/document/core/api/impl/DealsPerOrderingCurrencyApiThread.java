package com.document.core.api.impl;

import com.document.core.model.entity.DealsPerOrderingCurrency;
import com.document.core.model.enumconstant.Currency;
import com.document.core.model.enumconstant.ParameterConstant;
import com.document.core.model.repository.DealsPerOrderingCurrencyRepository;
import com.document.web.util.LoggerUtil;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DealsPerOrderingCurrencyApiThread implements Runnable {

    private DealsPerOrderingCurrency dealsPerOrderingCurrency;

    private DealsPerOrderingCurrencyRepository dealsPerOrderingCurrencyRepository;
    private DealsPerOrderingCurrencyRepository repository;

    public DealsPerOrderingCurrencyApiThread(DealsPerOrderingCurrencyRepository repository) {
        dealsPerOrderingCurrencyRepository = repository;
    }

    public DealsPerOrderingCurrencyApiThread(DealsPerOrderingCurrency dealsPerOrderingCurrency, DealsPerOrderingCurrencyRepository repository) {
        this.dealsPerOrderingCurrency = dealsPerOrderingCurrency;
        this.repository = repository;
    }

    //@Transactional(propagation = REQUIRES_NEW)
    public void save(Map<Currency, Long> map) {
        ExecutorService executor = Executors.newFixedThreadPool(ParameterConstant.threadSize);
        try {
            for (Currency currency : map.keySet()) {
                DealsPerOrderingCurrency dealsPerOrderingCurrency = dealsPerOrderingCurrencyRepository.findByOrderingCurrency(currency);

                if (dealsPerOrderingCurrency == null) {
                    dealsPerOrderingCurrency = new DealsPerOrderingCurrency();
                    dealsPerOrderingCurrency.setOrderingCurrency(currency);
                }

                dealsPerOrderingCurrency.setDealCount(dealsPerOrderingCurrency.getDealCount() + map.get(currency));


                DealsPerOrderingCurrencyApiThread obj = new DealsPerOrderingCurrencyApiThread(dealsPerOrderingCurrency, dealsPerOrderingCurrencyRepository);
                Thread thread = new Thread(obj);
                executor.execute(thread);
            }

            executor.shutdown();
            // Wait until all threads are finish
            while (!executor.isTerminated()) {
                Thread.sleep(10);
                System.out.println("file import still in progress...");
            }

        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.logException(this.getClass(), e);
        }
    }


    @Override
    public void run() {
        repository.save(dealsPerOrderingCurrency);
    }
}
