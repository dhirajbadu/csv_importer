package com.document.core.api.impl;

import com.document.core.api.iapi.IDealsPerOrderingCurrencyApi;
import com.document.core.model.entity.DealsPerOrderingCurrency;
import com.document.core.model.entity.ValidDocument;
import com.document.core.model.enumconstant.Currency;
import com.document.core.model.repository.DealsPerOrderingCurrencyRepository;
import com.document.core.util.ConvertUtil;
import com.document.web.util.LoggerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Service
public class DealsPerOrderingCurrencyApi implements IDealsPerOrderingCurrencyApi, Runnable {

    private DealsPerOrderingCurrency dealsPerOrderingCurrency;

    public DealsPerOrderingCurrencyApi() {
    }

    public DealsPerOrderingCurrencyApi(DealsPerOrderingCurrency dealsPerOrderingCurrency) {
        this.dealsPerOrderingCurrency = dealsPerOrderingCurrency;
    }

    @Autowired
    private DealsPerOrderingCurrencyRepository dealsPerOrderingCurrencyRepository;

    /**
     * this function is used to update current count into database
     *
     * @Param : List<ValidDocument>
     * @Return :
     */
    @Override
    public void updateOnDeal(List<ValidDocument> documentList) {

        try {
            Map<Currency, Long> map = getCountedCurrency(documentList);

            new DealsPerOrderingCurrencyApiThread(dealsPerOrderingCurrencyRepository).save(map);

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e);
        }
    }

    /**
     * this function is used to get count of Currency from validDocument list
     *
     * @Param : List<ValidDocument>
     * @Return : Map<Currency , Long>
     */
    public Map<Currency, Long> getCountedCurrency(List<ValidDocument> documentList) {
        Map<Currency, Long> countedMap = documentList.stream().collect(Collectors.groupingBy(ValidDocument::getFrmCurrencyISO, Collectors.counting()));

        return countedMap;
    }

    /**
     * this function takes pagination parameters and retrieves DealsPerOrderingCurrency list from database
     *
     * @Param : pagination parameters
     * @Return : DealsPerOrderingCurrency list
     */
    @Override
    public List<DealsPerOrderingCurrency> list(int max, int offset, String direction, String property) {
        property = ConvertUtil.getValidProperty(property, "id", "orderingCurrency", "dealCount");
        Pageable pageable = ConvertUtil.createOffsetPageRequest(max, offset, direction, property);
        return dealsPerOrderingCurrencyRepository.findAll(pageable).getContent();
    }

    /**
     * this function returns the row count from dealsPerOrderingCurrency table
     *
     * @Param :
     * @Return : long
     */
    @Override
    public long count() {
        return dealsPerOrderingCurrencyRepository.count();
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW)
    public void run() {
        try {
            dealsPerOrderingCurrencyRepository.save(dealsPerOrderingCurrency);
        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e);
        }
    }
}
