package com.document.core.model.dto;

import com.document.core.model.entity.AbstractEntity;
import com.document.core.model.entity.InvalidDocument;
import com.document.core.model.entity.ValidDocument;

import java.util.List;

public class DataResult extends AbstractEntity<Long> {

    private List<ValidDocument> validList;

    private List<InvalidDocument> inValidList;

    public List<ValidDocument> getValidList() {
        return validList;
    }

    public void setValidList(List<ValidDocument> validList) {
        this.validList = validList;
    }

    public List<InvalidDocument> getInValidList() {
        return inValidList;
    }

    public void setInValidList(List<InvalidDocument> inValidList) {
        this.inValidList = inValidList;
    }
}
