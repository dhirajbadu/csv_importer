package com.document.core.model.enumconstant;

public enum  Currency {

    //todo add all the valid currency here
   AFGHANISTAN ("AFN" , "971"),
   ALBANIA ("ALL" , "008"),
   ALGERIA("DZD" , "012"),
   AMERICAN ("USD" , "840"),
   ANDORRA("EUR" , "978"),
   ANGOLA("AOA" , "973"),
   ANGUILLA	("XCD" , "951"),
   ANTIGUA ("XCD" , "951"),
   ARGENTINA	("ARS" , "032"),
   ARMENIA	("AMD" , "051"),
   ARUBA	("AWG" , "533"),
   AUSTRALIA	("AUD" , "036"),
   AUSTRIA	("EUR" , "978"),
   AZERBAIJAN	("AZN" , "944"),
   BAHAMAS ("BSD" , "044"),
   BAHRAIN	("BHD" , "048"),
   BANGLADESH	("BDT" , "050"),
   BARBADOS	("BBD" , "052"),
   BELARUS	("BYR" , "974"),
   BELGIUM	("EUR" , "978"),
   BELIZE	("BZD" , "084"),
   BENIN	("XOF" , "952"),
   BERMUDA	("BMD" , "060"),
   BHUTAN_NGULTRUM	("BTN" , "064"),
   BHUTAN_INDIAN_RUPEE	("INR" , "356"),
   BOLIVIA_BOLIVIANO ("BOB" , "068"),
   BOLIVIA_MVDOL ("BOV" , "984"),
   BONAIRE("USD" , "840"),
   BOSNIA ("BAM" , "977"),
   BOTSWANA	("BWP" , "072"),
   BOUVET ("NOK" , "578"),
   BRAZIL	("BRL" , "986"),
   BRITISH ("USD" , "840"),
   BRUNEI ("BND" , "096"),
   BULGARIA	("BGN" , "975"),
   BURKINA ("XOF" , "952"),
   BURUNDI	("BIF" , "108"),
   CABO ("CVE" , "132"),
   CAMBODIA	("KHR" , "116"),
   CAMEROON	("XAF" , "950"),
   CANADA	("CAD" , "124"),
   CAYMAN ("KYD" , "136"),
   CENTRAL("XAF" , "950"),
   CHAD("XAF" , "950"),
   CHILE("CLF" , "990");

    private final String value;
    private final String code;

    Currency(String value, String code) {
        this.value = value;
        this.code = code;
    }

    @Override
    public String toString() {
        return value + "-" + code;
    }

    public String getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }


    public static Currency getEnum(String value) {
        if (value == null)
            return null;
        for (Currency v : values()) {
            //System.out.println(v.getValue() + " : " + v.getCode());
            if (value.equalsIgnoreCase(v.getValue()) || value.equals(v.getCode()))
                return v;
        }
        return null;
    }


 /* public static final String  CHILE	= "CLP	152";
  public static final String  CHINA	= "CNY	156";
  public static final String  CHRISTMAS = "AUD	036";
  public static final String  COCOS = "AUD	036";
  public static final String  COLOMBIA	= "Peso	COP	170
  public static final String  COLOMBIA	= "de Valor Real	COU	970
  public static final String  COMOROS = "THE)	Comoro Franc	KMF	174
  public static final String  CONGO = "THE DEMOCRATIC REPUBLIC OF THE)	Congolese Franc	CDF	976
  public static final String  CONGO = "THE)	CFA Franc BEAC	XAF	950
  public static final String  COOK = "(THE)	New Zealand Dollar	NZD	554
  public static final String  COSTA = "Costa Rican Colon	CRC	188
  public static final String  CROATIA	= "HRK	191
  public static final String  CUBA	= "Convertible	CUC	931
  public static final String  CUBA	= "Peso	CUP	192
  public static final String  CURAÇAO	= "Antillean Guilder	ANG	532
  public static final String  CYPRUS	= "EUR	978
  public static final String  CZECH = "(THE)	Czech Koruna	CZK	203
  public static final String  CÔTE = "IVOIRE	CFA Franc BCEAO	XOF	952
  public static final String  DENMARK	= "Krone	DKK	208
  public static final String  DJIBOUTI	= "Franc	DJF	262
  public static final String  DOMINICA	= "Caribbean Dollar	XCD	951
  public static final String  DOMINICAN = "(THE)	Dominican Peso	DOP	214
  public static final String  ECUADOR	= "Dollar	USD	840
  public static final String  EGYPT	= "Pound	EGP	818
  public static final String  EL = "El Salvador Colon	SVC	222
  public static final String  EL = "US Dollar	USD	840
  public static final String  EQUATORIAL = "CFA Franc BEAC	XAF	950
  public static final String  ERITREA	= "ERN	232
  public static final String  ESTONIA	= "EUR	978
  public static final String  ETHIOPIA	= "Birr	ETB	230
  public static final String  EUROPEAN = "Euro	EUR	978
  public static final String  FALKLAND = "(THE) [MALVINAS]	Falkland Islands Pound	FKP	238
  public static final String  FAROE = "(THE)	Danish Krone	DKK	208
  public static final String  FIJI	= "Dollar	FJD	242
  public static final String  FINLAND	= "EUR	978
  public static final String  FRANCE	= "EUR	978
  public static final String  FRENCH = "Euro	EUR	978
  public static final String  FRENCH = "CFP Franc	XPF	953
  public static final String  FRENCH = "TERRITORIES (THE)	Euro	EUR	978
  public static final String  GABON	= "Franc BEAC	XAF	950
  public static final String  GAMBIA = "THE)	Dalasi	GMD	270
  public static final String  GEORGIA	= "GEL	981
  public static final String  GERMANY	= "EUR	978
  public static final String  GHANA	= "Cedi	GHS	936
  public static final String  GIBRALTAR	= "Pound	GIP	292
  public static final String  GREECE	= "EUR	978
  public static final String  GREENLAND	= "Krone	DKK	208
  public static final String  GRENADA	= "Caribbean Dollar	XCD	951
  public static final String  GUADELOUPE	= "EUR	978
  public static final String  GUAM	= "Dollar	USD	840
  public static final String  GUATEMALA	= "GTQ	320
  public static final String  GUERNSEY	= "Sterling	GBP	826
  public static final String  GUINEA	= "Franc	GNF	324
  public static final String  GUINEA= "BISSAU	CFA Franc BCEAO	XOF	952
  public static final String  GUYANA	= "Dollar	GYD	328
  public static final String  HAITI	= "HTG	332
  public static final String  HAITI	= "Dollar	USD	840
  public static final String  HEARD = "AND McDONALD ISLANDS	Australian Dollar	AUD	036
  public static final String  HOLY = "(THE)	Euro	EUR	978
  public static final String  HONDURAS	= "HNL	340
  public static final String  HONG = "Hong Kong Dollar	HKD	344
  public static final String  HUNGARY	= "HUF	348
  public static final String  ICELAND	= "Krona	ISK	352
  public static final String  INDIA	= "Rupee	INR	356
  public static final String  INDONESIA	= "IDR	360
  public static final String  INTERNATIONAL = "FUND (IMF) 	SDR (Special Drawing Right)	XDR	960
  public static final String  IRAN = "ISLAMIC REPUBLIC OF)	Iranian Rial	IRR	364
  public static final String  IRAQ	= "Dinar	IQD	368
  public static final String  IRELAND	= "EUR	978
  public static final String  ISLE = "MAN	Pound Sterling	GBP	826
  public static final String  ISRAEL	= "Israeli Sheqel	ILS	376
  public static final String  ITALY	= "EUR	978
  public static final String  JAMAICA	= "Dollar	JMD	388
  public static final String  JAPAN	= "JPY	392
  public static final String  JERSEY	= "Sterling	GBP	826
  public static final String  JORDAN	= "Dinar	JOD	400
  public static final String  KAZAKHSTAN	= "KZT	398
  public static final String  KENYA	= "Shilling	KES	404
  public static final String  KIRIBATI	= "Dollar	AUD	036
  public static final String  KOREA = "THE DEMOCRATIC PEOPLE’S REPUBLIC OF)	North Korean Won	KPW	408
  public static final String  KOREA = "THE REPUBLIC OF)	Won	KRW	410
  public static final String  KUWAIT	= "Dinar	KWD	414
  public static final String  KYRGYZSTAN	= "KGS	417
  public static final String  LAO = "’S DEMOCRATIC REPUBLIC (THE)	Kip	LAK	418
  public static final String  LATVIA	= "EUR	978
  public static final String  LEBANON	= "Pound	LBP	422
  public static final String  LESOTHO	= "LSL	426
  public static final String  LESOTHO	= "ZAR	710
  public static final String  LIBERIA	= "Dollar	LRD	430
  public static final String  LIBYA	= "Dinar	LYD	434
  public static final String  LIECHTENSTEIN	= "Franc	CHF	756
  public static final String  LITHUANIA	= "EUR	978
  public static final String  LUXEMBOURG	= "EUR	978
  public static final String  MACAO	= "MOP	446
  public static final String  MACEDONIA = "THE FORMER YUGOSLAV REPUBLIC OF)	Denar	MKD	807
  public static final String  MADAGASCAR	= "Ariary	MGA	969
  public static final String  MALAWI	= "MWK	454
  public static final String  MALAYSIA	= "Ringgit	MYR	458
  public static final String  MALDIVES	= "MVR	462
  public static final String  MALI	= "Franc BCEAO	XOF	952
  public static final String  MALTA	= "EUR	978
  public static final String  MARSHALL = "(THE)	US Dollar	USD	840
  public static final String  MARTINIQUE	= "EUR	978
  public static final String  MAURITANIA	= "MRO	478
  public static final String  MAURITIUS	= "Rupee	MUR	480
  public static final String  MAYOTTE	= "EUR	978
  public static final String  MEMBER = "OF THE AFRICAN DEVELOPMENT BANK GROUP	ADB Unit of Account	XUA	965
  public static final String  MEXICO	= "Peso	MXN	484
  public static final String  MEXICO	= "Unidad de Inversion (UDI)	MXV	979
  public static final String  MICRONESIA = "FEDERATED STATES OF)	US Dollar	USD	840
  public static final String  MOLDOVA = "THE REPUBLIC OF)	Moldovan Leu	MDL	498
  public static final String  MONACO	= "EUR	978
  public static final String  MONGOLIA	= "MNT	496
  public static final String  MONTENEGRO	= "EUR	978
  public static final String  MONTSERRAT	= "Caribbean Dollar	XCD	951
  public static final String  MOROCCO	= "Dirham	MAD	504
  public static final String  MOZAMBIQUE	= "Metical	MZN	943
  public static final String  MYANMAR	= "MMK	104
  public static final String  NAMIBIA	= "Dollar	NAD	516
  public static final String  NAMIBIA	= "ZAR	710
  public static final String  NAURU	= "Dollar	AUD	036
  public static final String  NEPAL	= "Rupee	NPR	524
  public static final String  NETHERLANDS = "THE)	Euro	EUR	978
  public static final String  NEW = "CFP Franc	XPF	953
  public static final String  NEW = "New Zealand Dollar	NZD	554
  public static final String  NICARAGUA	= "Oro	NIO	558
  public static final String  NIGER = "THE)	CFA Franc BCEAO	XOF	952
  public static final String  NIGERIA	= "NGN	566
  public static final String  NIUE	= "Zealand Dollar	NZD	554
  public static final String  NORFOLK = "Australian Dollar	AUD	036
  public static final String  NORTHERN = "ISLANDS (THE)	US Dollar	USD	840
  public static final String  NORWAY	= "Krone	NOK	578
  public static final String  OMAN	= "Omani	OMR	512
  public static final String  PAKISTAN	= "Rupee	PKR	586
  public static final String  PALAU	= "Dollar	USD	840
  public static final String  PALESTINE= "STATE OF	No universal currency
  public static final String  PANAMA	= "PAB	590
  public static final String  PANAMA	= "Dollar	USD	840
  public static final String  PAPUA = "GUINEA	Kina	PGK	598
  public static final String  PARAGUAY	= "PYG	600
  public static final String  PERU	= "Sol	PEN	604
  public static final String  PHILIPPINES = "THE)	Philippine Peso	PHP	608
  public static final String  PITCAIRN	= "Zealand Dollar	NZD	554
  public static final String  POLAND	= "PLN	985
  public static final String  PORTUGAL	= "EUR	978
  public static final String  PUERTO = "US Dollar	USD	840
  public static final String  QATAR	= "Rial	QAR	634
  public static final String  ROMANIA	= "Leu	RON	946
  public static final String  RUSSIAN = "(THE)	Russian Ruble	RUB	643
  public static final String  RWANDA	= "Franc	RWF	646
  public static final String  RÉUNION	= "EUR	978
  public static final String  SAINT = "Euro	EUR	978
  public static final String  SAINT = "", ASCENSION AND TRISTAN DA CUNHA	Saint Helena Pound	SHP	654
  public static final String  SAINT = "AND NEVIS	East Caribbean Dollar	XCD	951
  public static final String  SAINT = "East Caribbean Dollar	XCD	951
  public static final String  SAINT = "(FRENCH PART)	Euro	EUR	978
  public static final String  SAINT = "AND MIQUELON	Euro	EUR	978
  public static final String  SAINT = "AND THE GRENADINES	East Caribbean Dollar	XCD	951
  public static final String  SAMOA	= "WST	882
  public static final String  SAN = "Euro	EUR	978
  public static final String  SAO = "AND PRINCIPE	Dobra	STD	678
  public static final String  SAUDI = "Saudi Riyal	SAR	682
  public static final String  SENEGAL	= "Franc BCEAO	XOF	952
  public static final String  SERBIA	= "Dinar	RSD	941
  public static final String  SEYCHELLES	= "Rupee	SCR	690
  public static final String  SIERRA = "Leone	SLL	694
  public static final String  SINGAPORE	= "Dollar	SGD	702
  public static final String  SINT = "(DUTCH PART)	Netherlands Antillean Guilder	ANG	532
  public static final String  SISTEMA = "DE COMPENSACION REGIONAL DE PAGOS "SUCRE"	Sucre	XSU	994
  public static final String  SLOVAKIA	= "EUR	978
  public static final String  SLOVENIA	= "EUR	978
  public static final String  SOLOMON = "Solomon Islands Dollar	SBD	090
  public static final String  SOMALIA	= "Shilling	SOS	706
  public static final String  SOUTH = "Rand	ZAR	710
  public static final String  SOUTH = "AND THE SOUTH SANDWICH ISLANDS	No universal currency
  public static final String  SOUTH = "South Sudanese Pound	SSP	728
  public static final String  SPAIN	= "EUR	978
  public static final String  SRI = "Sri Lanka Rupee	LKR	144
  public static final String  SUDAN = "THE)	Sudanese Pound	SDG	938
  public static final String  SURINAME	= "Dollar	SRD	968
  public static final String  SVALBARD = "JAN MAYEN	Norwegian Krone	NOK	578
  public static final String  SWAZILAND	= "SZL	748
  public static final String  SWEDEN	= "Krona	SEK	752
  public static final String  SWITZERLAND	= "Euro	CHE	947
  public static final String  SWITZERLAND	= "Franc	CHF	756
  public static final String  SWITZERLAND	= "Franc	CHW	948
  public static final String  SYRIAN = "REPUBLIC	Syrian Pound	SYP	760
  public static final String  TAIWAN = "PROVINCE OF CHINA)	New Taiwan Dollar	TWD	901
  public static final String  TAJIKISTAN	= "TJS	972
  public static final String  TANZANIA= "UNITED REPUBLIC OF	Tanzanian Shilling	TZS	834
  public static final String  THAILAND	= "THB	764
  public static final String  TIMOR= "LESTE	US Dollar	USD	840
  public static final String  TOGO	= "Franc BCEAO	XOF	952
  public static final String  TOKELAU	= "Zealand Dollar	NZD	554
  public static final String  TONGA	= "’anga	TOP	776
  public static final String  TRINIDAD = "TOBAGO	Trinidad and Tobago Dollar	TTD	780
  public static final String  TUNISIA	= "Dinar	TND	788
  public static final String  TURKEY	= "Lira	TRY	949
  public static final String  TURKMENISTAN	= "New Manat	TMT	934
  public static final String  TURKS = "CAICOS ISLANDS (THE)	US Dollar	USD	840
  public static final String  TUVALU	= "Dollar	AUD	036
  public static final String  UGANDA	= "Shilling	UGX	800
  public static final String  UKRAINE	= "UAH	980
  public static final String  UNITED = "EMIRATES (THE)	UAE Dirham	AED	784
  public static final String  UNITED = "OF GREAT BRITAIN AND NORTHERN IRELAND (THE)	Pound Sterling	GBP	826
  public static final String  UNITED = "MINOR OUTLYING ISLANDS (THE)	US Dollar	USD	840
  public static final String  UNITED = "OF AMERICA (THE)	US Dollar	USD	840
  public static final String  UNITED = "OF AMERICA (THE)	US Dollar (Next day)	USN	997
  public static final String  URUGUAY	= "Peso en Unidades Indexadas (URUIURUI)	UYI	940
  public static final String  URUGUAY	= "Uruguayo	UYU	858
  public static final String  UZBEKISTAN	= "Sum	UZS	860
  public static final String  VANUATU	= "VUV	548
  public static final String  VENEZUELA = "BOLIVARIAN REPUBLIC OF)	Bolivar	VEF	937
  public static final String  VIET = "Dong	VND	704
  public static final String  VIRGIN = "(BRITISH)	US Dollar	USD	840
  public static final String  VIRGIN = "(U.S.)	US Dollar	USD	840
  public static final String  WALLIS = "FUTUNA	CFP Franc	XPF	953
  public static final String  WESTERN = "Moroccan Dirham	MAD	504
  public static final String  YEMEN	= "Rial	YER	886
  public static final String  ZAMBIA	= "Kwacha	ZMW	967
  public static final String  ZIMBABWE	= "Dollar	ZWL	932public static final StringÅLAND ISLANDS	Euro	EUR	978*/
}
