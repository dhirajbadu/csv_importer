package com.document.core.model.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "table_invalid_document")
public class InvalidDocument extends AbstractEntity<Long> {

    private String dealId;

    private String frmCurrencyISO;

    private String toCurrencyISO;

    private String timestamp;

    private String amount;

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFrmCurrencyISO() {
        return frmCurrencyISO;
    }

    public void setFrmCurrencyISO(String frmCurrencyISO) {
        this.frmCurrencyISO = frmCurrencyISO;
    }

    public String getToCurrencyISO() {
        return toCurrencyISO;
    }

    public void setToCurrencyISO(String toCurrencyISO) {
        this.toCurrencyISO = toCurrencyISO;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }
}
