package com.document.core.model.entity;

import com.document.core.model.enumconstant.Currency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "table_valid_document")
public class ValidDocument extends AbstractEntity<Long>{

    @Column(unique = true , nullable = false)
    private String dealId;

    @Column(nullable = false)
    private Currency frmCurrencyISO;

    @Column(nullable = false)
    private Currency toCurrencyISO;

    @Column(nullable = false)
    private Long timestamp;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private String fileName;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public Currency getFrmCurrencyISO() {
        return frmCurrencyISO;
    }

    public void setFrmCurrencyISO(Currency frmCurrencyISO) {
        this.frmCurrencyISO = frmCurrencyISO;
    }

    public Currency getToCurrencyISO() {
        return toCurrencyISO;
    }

    public void setToCurrencyISO(Currency toCurrencyISO) {
        this.toCurrencyISO = toCurrencyISO;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
