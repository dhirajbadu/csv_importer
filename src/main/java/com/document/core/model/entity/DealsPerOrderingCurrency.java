package com.document.core.model.entity;

import com.document.core.model.enumconstant.Currency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "table_deals_per_ordering_currency")
public class DealsPerOrderingCurrency extends AbstractEntity<Long>{

    @Column(nullable = false , unique = true)
    private Currency orderingCurrency;

    private long dealCount;

    public Currency getOrderingCurrency() {
        return orderingCurrency;
    }

    public void setOrderingCurrency(Currency orderingCurrency) {
        this.orderingCurrency = orderingCurrency;
    }

    public long getDealCount() {
        return dealCount;
    }

    public void setDealCount(long dealCount) {
        this.dealCount = dealCount;
    }
}
