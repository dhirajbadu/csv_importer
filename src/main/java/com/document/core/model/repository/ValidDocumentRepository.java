package com.document.core.model.repository;

import com.document.core.model.entity.ValidDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidDocumentRepository extends JpaRepository<ValidDocument , Long>{

    @Query("SELECT CASE WHEN COUNT(v) > 0 THEN true ELSE false END FROM ValidDocument v WHERE v.dealId = :dealId")
    boolean existsDistinctByDealId(@Param("dealId")String dealId);

    @Query("SELECT CASE WHEN COUNT(v) > 0 THEN true ELSE false END FROM ValidDocument v WHERE v.fileName = :filename")
    boolean existsDistinctByFileName(@Param("filename")String filename);
}
