package com.document.core.model.repository;

import com.document.core.model.entity.DealsPerOrderingCurrency;
import com.document.core.model.enumconstant.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DealsPerOrderingCurrencyRepository extends JpaRepository<DealsPerOrderingCurrency , Long> {

    @Query("SELECT CASE WHEN COUNT(oc) > 0 THEN true ELSE false END FROM DealsPerOrderingCurrency oc WHERE oc.orderingCurrency = :currency")
    boolean existsDistinctByOrderingCurrency(@Param("currency")Currency currency);

    DealsPerOrderingCurrency findByOrderingCurrency(Currency orderingCurrency);

}
