<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 11/20/18
  Time: 8:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Document | Management</title>
    <meta
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
            name="viewport">
    <!-- Bootstrap 3.3.7 -->

    <%--
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ionicons.min.css">
        <!-- Theme style -->
        <!-- select2 -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/select2.css">
        <!-- custom -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/custom.css">
        <!-- datatable -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.dataTables.min.css">
        <!-- bootstrap3-wysihtml5 -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap3-wysihtml5.min.css">
        <!-- date picker -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-datepicker.min.css">


            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
     --%>


    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/Party/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/Party/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/Party/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/Party/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/Party/css/util.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/Party/css/main.css">

    <!-- datatable -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.dataTables.min.css">
    <script src="${pageContext.request.contextPath}/resources/Party/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="${pageContext.request.contextPath}/resources/Party/vendor/bootstrap/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/Party/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="${pageContext.request.contextPath}/resources/Party/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="${pageContext.request.contextPath}/resources/Party/js/main.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async
            src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

    <script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');


        $(function () {
            $.fn.dataTable.ext.errMode = 'none';
            $('#ajaxTableValid').on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                "processing": true,
                "serverSide": true,
                'searching': false,
                'ordering': true,
                'lengthChange': true,
                "ajax": $('#ajaxTableValid').attr('url')
            });

            $('#ajaxTableInvalid').on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                "processing": true,
                "serverSide": true,
                'searching': false,
                'ordering': true,
                'lengthChange': true,
                "ajax": $('#ajaxTableInvalid').attr('url')
            });

            //ajaxTableDealCount

            $('#ajaxTableDealCount').on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                "processing": true,
                "serverSide": true,
                'searching': false,
                'ordering': true,
                'lengthChange': true,
                "ajax": $('#ajaxTableDealCount').attr('url')
            });

            $('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        })
    </script>

    <style>
        .file-upload {
            position: relative;
            display: inline-block;
        }

        .file-upload__label {
            display: block;
            padding: 1em 2em;
            color: #fff;
            background: #222;
            border-radius: .4em;
            transition: background .3s;
        }

        .file-upload__label:hover {
            cursor: pointer;
            background: #000;
        }

        .file-upload__input {
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            font-size: 1;
            width: 0;
            height: 100%;
            opacity: 0;
        }
    </style>

</head>
<body>
<div class="container">
    <div class="content">

        <div class="limiter">
            <div class="container-table100">


                <div class="col-lg-12" style="text-align:center;">
                    <h2>Document Management System</h2>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert"
                                   aria-label="close">x</a> <strong>${message}</strong>
                            </div>
                        </c:if>

                        <c:if test="${not empty error}">
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert"
                                   aria-label="close">x</a> <strong>${error}</strong>
                            </div>
                        </c:if>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <form action="${pageContext.request.contextPath}/upload"
                              method="post" enctype="multipart/form-data">

                            <div class="file-upload">
                                <label for="upload" class="file-upload__label ">Upload Cvs File</label>
                                <input type="file" id="upload" class="file-upload__input" name="doc"
                                       placeholder="choose csv file" required>
                            </div>


                            <label class="control-label"></label>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-upload"></i>&nbsp; Import
                            </button>


                        </form>

                    </div>
                </div>

                <div class="wrap-table100">
                    <h3><span class="label label-default">Valid List</span></h3>
                    <div class="table100 ver1 m-b-110">
                        <table id="ajaxTableValid" data-vertable="ver1"
                               url="${pageContext.request.contextPath}/ajax/document/validlist">
                            <thead>
                            <tr class="row100 head">
                                <th class="column100 column1" data-column="column1">Id</th>
                                <th class="column100 column2" data-column="column2">frmCurrencyISO</th>
                                <th class="column100 column3" data-column="column3">toCurrencyISO</th>
                                <th class="column100 column4" data-column="column4">timestamp</th>
                                <th class="column100 column5" data-column="column5">amount</th>
                                <th class="column100 column5" data-column="column6">filename</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="wrap-table100">
                    <h3><span class="label label-default">Invalid List</span></h3>
                    <div class="table100 ver1 m-b-110">
                        <table id="ajaxTableInvalid" data-vertable="ver1"
                               url="${pageContext.request.contextPath}/ajax/document/invalidlist">
                            <thead>
                            <tr class="row100 head">
                                <th class="column100 column1" data-column="column1">Id</th>
                                <th class="column100 column2" data-column="column2">frmCurrencyISO</th>
                                <th class="column100 column3" data-column="column3">toCurrencyISO</th>
                                <th class="column100 column4" data-column="column4">timestamp</th>
                                <th class="column100 column5" data-column="column5">amount</th>
                                <th class="column100 column5" data-column="column6">filename</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="wrap-table100">
                    <h3><span class="label label-default">Deals As Per Ordering Currency</span></h3>
                    <div class="table100 ver1 m-b-110">
                        <table id="ajaxTableDealCount" data-vertable="ver1"
                               url="${pageContext.request.contextPath}/ajax/document/dealcount">
                            <thead>
                            <tr class="row100 head">
                                <th class="column100 column1" data-column="column1">Id</th>
                                <th class="column100 column2" data-column="column2">CurrencyISO</th>
                                <th class="column100 column3" data-column="column3">Total Deals</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>

<%-- <!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Slim Scroll -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>
<!-- select2-->
<script src="${pageContext.request.contextPath}/resources/js/select2.full.min.js"></script>
<!-- datatable-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<!-- custom script-->
<!-- datepicker-->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap3-wysihtml5-->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap3-wysihtml5.all.min.js"></script> --%>

</body>
</html>
